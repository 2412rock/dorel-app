/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from "react";
import type {Node} from 'react';
import ListItemsScreen from './components/ListItemsScreen'
import DetailsScreen from './components/DetailsScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


class App extends Component {

state = {
  selectedJudet: ""
}

  constructor(){
    super();
  }
  
  
  render(){
  const Stack = createNativeStackNavigator();

    return (
      <NavigationContainer>
        <Stack.Navigator>
          {/* <Stack.Screen name="SearchScreen" component={SearchScreen} options={{ title: 'Search screen' }}/> */}
          <Stack.Screen name="ListItemScreen" component={ListItemsScreen} options={{ title: 'Dorel' , headerShown: false}} />
          <Stack.Screen name="DetailsScreen" component={DetailsScreen} options={{ title: 'Details screen' }}/>
        </Stack.Navigator>
    </NavigationContainer>
    );
     
  }
  
};



export default App;
