import React, {Component} from 'react';
import {SelectList} from 'react-native-dropdown-select-list';
import {ScrollView, StyleSheet, Text, View, Button} from 'react-native';
import axios from 'axios';

import {useNavigation} from '@react-navigation/native';

class SearchBars extends Component {
  judete = [
    {key: '1', value: 'Huneodara'},
    {key: '2', value: 'Arad'},
    {key: '3', value: 'Timisoara'},
    {key: '4', value: 'Bucuresti', disabled: true},
    {key: '5', value: 'Cluj'},
  ];

  joburi = [
    {key: '1', value: 'Gradinar'},
    {key: '2', value: 'Zugrav'},
    {key: '3', value: 'Instalator'},
  ];

  constructor() {
    super();
    this.fetchData();
  }

  onPressSearch() {
    console.log('PRESSED SEARCH');
    const {navigation} = this.props;
    console.log('Pressed search');
    console.log({
      selectedJudet: this.selectedJudet,
      selectedJob: this.selectedJob,
    });
    navigation.navigate('ListItemScreen', {
      selectedJudet: this.selectedJudet,
      selectedJob: this.selectedJob,
    });
  }

  setSelectedJudet(selected) {
    console.log('SELECTED JUDET ' + selected);
    // this.setState({ selectedJudet: selected });
    this.selectedJudet = selected;
  }

  setSelectedJob(selected) {
    console.log('SELECTED JOB ' + selected);
    // this.setState({ selectedJudet: selected });
    this.selectedJob = selected;
  }

  fetchData() {
    axios
      .get('http://localhost:51233/myPage')
      .then(response => {
        console.log('GOT RESPONSE');
        //console.log(response);
        // Store Values in Temporary Array
        console.log(response.data.jobs);
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    return (
      <View style={styles.dropdownsContainer}>
        <View style={styles.firstDropdown}>
          <SelectList
            setSelected={val => {
              this.setSelectedJudet(val);
            }}
            data={this.judete}
            save="value"
            placeholder="Alege un judet"
            searchPlaceholder="Alege un judet"
          />
        </View>
        <View style={styles.secondDropdown}>
          <SelectList
            setSelected={val => {
              this.setSelectedJob(val);
            }}
            data={this.joburi}
            save="value"
            placeholder="Alege un job"
            searchPlaceholder="Alege un job"
          />
        </View>
        {/* <View style = {styles.secondDropdown}>
              <SelectList setSelected={setSelected} data={data} onSelect={() => console.log("Data selected")} />
            </View> */}
        {/* <Button
          onPress={() => {
            this.onPressSearch();
          }}
          title="Search"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dropdownsContainer: {
    alignItems: 'center',
    //flex: 1,
    justifyContent: 'center',
    //paddingHorizontal: 24,
  },
  firstDropdown: {
    width: '90%',
  },
  secondDropdown: {
    marginTop: 10,
    width: '90%',
  },
});

// Wrap and export
export default function (props) {
  const navigation = useNavigation();

  return <SearchBars {...props} navigation={navigation} />;
}
