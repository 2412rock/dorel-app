import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  SafeAreaView,
  FlatList,
  Image,
  TouchableHighlight,
} from 'react-native';
import axios from 'axios';
import SearchBars from './SearchBars';
import StarRating from 'react-native-star-rating-widget';
import Icon from 'react-native-vector-icons/MaterialIcons';

interface ListItemScreenProps {
  navigation: any;
}

class ListItemsScreen extends Component {
  constructor(props) {
    super(props);
    //console.log('Received selected values');
    //this.selectedJudet = this.props.route.params.selectedJudet;
    //this.selectedJob = this.props.route.params.selectedJob;
    //console.log(this.selectedJudet);
   // console.log(this.selectedJob);
    this.fetchData();
    this.listItems = [
      {_id: '0'},
      {_id: '1',
       'name': 'Ioan Jude',
       'job':'Instalator',
       'judet': 'Arad',
       'oras': 'Socodor',
       'stars': 3,
       'cardImage':'https://www.thespruce.com/thmb/UF4uHS3uU7SHfFogqbw9-1VzoO8=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/finding-a-good-plumber-4769651-hero-ad95546b6fb545d88714d909db4d7e16.jpg'},
       {_id: '2',
       'name': 'Cristi Dragomir',
       'job':'Instalator',
       'judet': 'Arad',
       'oras': 'Ghioroc',
       'stars': 3.5,
       'cardImage': 'https://bundeangajat.olx.ro/wp-content/uploads/2019/01/olx-instalator_530859598.jpg'},
       {_id: '3',
       'name': 'Teodor Dosa',
       'job':'Instalator',
       'judet': 'Hunedoara',
       'oras': 'Deva',
       'stars': 4,
       'cardImage': 'https://www.mesterulcasei.com.ro/wp-content/uploads/2018/10/instalator-sanitar.jpg'}];
    this.counter = 3;
  }

  fetchData() {
    axios
      .post(
        `http://localhost:51233/getJobsForQuery`,
        {judet: this.selectedJudet, job: this.selectedJob},
        {
          headers: {'Content-Type': 'application/json'},
        },
      )
      .then(response => {
        //console.log('RECEIVED RESPONSE AT POST');
        //console.log(response.data);
      })
      .catch(error => {
        //console.log('ERROR AT POST REQUEST');
        //console.log(error);
      });
  }

  loadMoreResults() {
    //console.log('LOAD MORE ITEMS');
    this.counter++;
    this.listItems.push({_id: this.counter.toString(),
    'name': 'Cristi Dragomir',
    'job':'Instalator',
    'judet': 'Arad',
    'oras': 'Ghioroc',
    'stars': 3,
    'cardImage': 'https://bundeangajat.olx.ro/wp-content/uploads/2019/01/olx-instalator_530859598.jpg'});
    // this.counter++;
    // this.listItems.push({_id: this.counter.toString()});
    // this.counter++;
    // this.listItems.push({_id: this.counter.toString()});
  }

  onPressItem(item) {
    //console.log('PRESSED ITEM');
    //console.log(item);
    //console.log('PRESSED SEARCH');
    const {navigation} = this.props;

    navigation.navigate('DetailsScreen', {
      item: item,
    });
  }

  render() {
    totalItems = 5;
    loadingMore = false;
    const image = {uri: 'https://reactjs.org/logo-og.png'};

    return (
      <SafeAreaView style={{flex: 1}}>
        <FlatList
         // contentContainerStyle={styles.list}
          // ListHeaderComponent={
          //   <View style={styles.header}>
          //     <Text style={styles.title}>Displaying {totalItems} Items</Text>
          //   </View>
          // }
          ListFooterComponent={
            <View style={styles.footer}>
              {loadingMore && (
                <Text style={styles.footerText}>Loading More...</Text>
              )}
            </View>
          }
          scrollEventThrottle={250}
          onEndReached={info => {
            this.loadMoreResults(info);
          }}
          onEndReachedThreshold={0.01}
          data={this.listItems}
          keyExtractor={item => 'item_' + item._id}
          renderItem={({item, index}) => {
            //console.log(item)
            if(index==0){
              // Search bars
              return (<View style={{marginBottom: 10, marginTop: 5}}>
                <SearchBars />
              </View>);
            }
            // Cards
            return (
              <React.Fragment key={index}>
                <TouchableHighlight
                  onPress={() => {
                    this.onPressItem(item);
                  }}
                  underlayColor="white">
                  {/* Item image */}
                  <View style={styles.item}>
                    <Image
                      style={styles.image}
                      source={{
                        uri: item.cardImage,
                      }}
                    />
                    {/* Item information */}
                    <View style={styles.itemInfo}>
                      <Text style={styles.name}>{item.name}</Text>
                      <Text>{item.job}</Text>
                      {/* Location info */}
                      <View style={styles.locationInCard}>
                        <Icon name="location-pin" size={30} color="green" />
                        <Text>{item.judet + " - " + item.oras}</Text>
                      </View>
                      <View>
                        <StarRating
                          rating={item.stars}
                          color={"#e07014"}
                          starSize ={18}
                          onChange={number => {
                            rating = number;
                          }}
                        />
                      </View>
                    </View>
                    {/* <Text>Item {item._id}</Text> */}
                  </View>
                </TouchableHighlight>
              </React.Fragment>
            );
          }}
        />
      </SafeAreaView>
    );
  }
}

export default ListItemsScreen;

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  image: {
    width: 200,
    height: 250,
    //borderRadius:10,
    //borderBottomLeftRadius: 10,
    //borderTopLeftRadius: 10,
    borderRadius: 10,
    resizeMode: 'cover',
    margin:10
  },
  name: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  title: {
    fontSize: 26,
    fontWeight: '600',
  },
  itemInfo: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-around',
  },
  item: {
    //paddingHorizontal: 15,
    backgroundColor: '#e3e7e8',
    //paddingVertical: '25%',
    //borderBottomWidth: 1,
    //borderColor: '#ccc',
    width: '90%',
    marginLeft: '5%',
    marginBottom: '7%',
    flexDirection: 'row',
    borderRadius: 10,
    overflow: 'hidden',
  },
  footer: {
    padding: 15,
  },
  footerText: {
    fontWeight: '600',
  },
  locationInCard :{
    alignItems: "center"
  }
});
