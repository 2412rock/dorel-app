import React, {Component} from 'react';
import {View, Image, StyleSheet, Text} from 'react-native';
import StarRating from 'react-native-star-rating-widget';

class ReviewerItem extends Component {
  constructor(props) {
    super(props);
    this.item = props.item;
  }

  render() {
    return (
      <View style={{marginLeft: 10, marginRight: 10, marginBottom: 20}}>
        {/* Profile pic, name and rating */}
        <View style={{flexDirection: 'row', marginTop: 0, marginBottom: 10}}>
          <Image
            style={styles.reviewerImage}
            source={{
              uri: this.item.profilePic,
            }}
          />
          <View style={styles.reviewerNameAndStars}>
            <Text style={styles.reviewerName}>{this.item.name}</Text>
            <View style={{marginRight: 0}}>
              <StarRating
                rating={this.item.stars}
                color={'#e07014'}
                starSize={18}
                onChange={number => {
                  rating = number;
                }}
              />
            </View>
          </View>
          <Text>3 Months agos</Text>
        </View>
        {/* Reviewer text */}
        <View style={styles.reviewerText}>
          <Text>{this.item.reviewText}</Text>
        </View>
      </View>
    );
  }
}

export default ReviewerItem;

const styles = StyleSheet.create({
  reviewerName: {
    fontWeight: 'bold',
    //marginLeft: 10,
    //marginRight:130

  },
  reviewerImage: {
    width: 35,
    height: 35,
    //borderRadius:10,
    //borderBottomLeftRadius: 10,
    //borderTopLeftRadius: 10,
    borderRadius: 400 / 2,
    resizeMode: 'cover',
    //marginLeft:10
  },

  reviewerText: {
    // marginLeft: 10,
    // marginRight: 10
  },
  reviewerNameAndStars: {
    flexDirection: 'column',
    //marginBottom: 15
    // justifyContent: 'center',
    // alignItems: 'center',
    //backgroundColor: 'red'
  },
});
