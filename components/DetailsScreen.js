import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  SafeAreaView,
  FlatList,
  Image,
  TouchableHighlight,
  TouchableNativeFeedback,
  Platform,
  Linking,
} from 'react-native';
import axios from 'axios';
import StarRating from 'react-native-star-rating-widget';
import ReviewerItem from './ReviewerItem';

class DetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.selectedItem = this.props.route.params.item;
    console.log('SELECTED ITEM');
    console.log(this.selectedItem);
    this.fetchImages();
    this.setInitialReviews();
  }

  setInitialReviews() {
    this.reviewsData = [
      {
        _id: '0',
        name: 'Andrew John',
        stars: '5',
        reviewText:
          'tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum dolore eu fugiatnulla pariatur. Excepteur sint occaecat cupidatat no',
        profilePic:
          'https://www.incimages.com/uploaded_files/image/1920x1080/getty_481292845_77896.jpg',
        timeOfReview: '3 days ago',
      },
      {
        _id: '1',
        name: 'Andrew John',
        stars: '5',
        reviewText:
          'tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum dolore eu fugiatnulla pariatur. Excepteur sint occaecat cupidatat no',
        profilePic:
          'https://www.incimages.com/uploaded_files/image/1920x1080/getty_481292845_77896.jpg',
        timeOfReview: '3 days ago',
      },
      {
        _id: '2',
        name: 'Andrew John',
        stars: '5',
        reviewText:
          'tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nostrud exercitation ullamco laborisnisi ut aliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum dolore eu fugiatnulla pariatur. Excepteur sint occaecat cupidatat no',
        profilePic:
          'https://www.incimages.com/uploaded_files/image/1920x1080/getty_481292845_77896.jpg',
        timeOfReview: '3 days ago',
      },
    ];
  }

  loadMoreReviews() {}

  fetchImages() {
    this.images = [
      'https://www.theplumberguy.com/wp-content/uploads/2022/07/plumbing-company-surprise-az.jpg',
      'https://kayplumbing.com/wp-content/uploads/2020/11/plumbing-issues-1-v1-f-2020.jpg',
      'https://www.nextinsurance.com/wp-content/uploads/2022/02/feb-2022-5.jpg',
      'https://www.theplumberguy.com/wp-content/uploads/2022/07/plumbing-company-surprise-az.jpg',
    ];
  }

  onPressPhoneNumber() {
    let phoneNumber = '';

    // isf (Platform.OS === 'android') {
    phoneNumber = 'tel:0123456789';
    // } else {
    //     phoneNumber = 'telprompt:0123456789';
    // }

    Linking.openURL(phoneNumber);
  }

  render() {
    rating = 5;
    return (
      <View>
        <ScrollView>
          <View style={styles.userDetails}>
            <Image
              source={{
                uri: 'https://geersplumbing.net/wp-content/uploads/2022/07/7-Essential-Characteristics-That-All-Plumbers-Must-Have-_-Plumber-in-North-Las-Vegas-NV.jpg',
              }}
              style={styles.profilePic}
            />
            <View style={styles.nameView}>
              <Text style={styles.name}>Ioan Jude</Text>
              <Text style={styles.functie}>Instalator</Text>
              <View style={styles.starRating}>
                <StarRating
                  rating={rating}
                  starSize={24}
                  onChange={number => {
                    rating = number;
                  }}
                />
              </View>
            </View>
          </View>
          <View style={styles.body}>
            <Text style={styles.descriptionTitle}>Description</Text>
            <Text style={styles.descriptionBody}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Text>
            <View>
              <ScrollView horizontal={true}>
                {this.images.map((value, index) => {
                  console.log(value);
                  return (
                    <Image
                      source={{
                        uri: value,
                      }}
                      style={styles.lucrareImage}
                    />
                  );
                })}
              </ScrollView>
            </View>
            <View>
              <Text style={styles.descriptionTitle}>Contact details</Text>
              <TouchableNativeFeedback
                onPress={() => {
                  this.onPressPhoneNumber();
                }}>
                <Text style={{fontSize: 17, textDecorationLine: 'underline'}}>
                  0745 977 987
                </Text>
              </TouchableNativeFeedback>
              <Text style={styles.descriptionTitle}>Reviews</Text>
            </View>
          </View>
          {/* Reviews items */}
          <View>
            {/* <FlatList
              nestedScrollEnabled
              scrollEventThrottle={250}
              // onEndReached={info => {
              //   this.loadMoreReviews(info);
              // }}
              onEndReachedThreshold={0.01}
              data={this.reviewsData}
              keyExtractor={item => 'item_' + item._id}
              renderItem={({item, index}) => {
                console.log(item);
                return (
                    <ReviewerItem item={item}/>
                );
              }}></FlatList> */}
              {
                this.reviewsData.map(el => {
                    console.log("MAP")
                    console.log(el)
                    return (<ReviewerItem key={el._id} item={el}></ReviewerItem>)
                })
              }
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default DetailsScreen;

const styles = StyleSheet.create({
  userDetails: {
    flexDirection: 'row',
  },
  reviewerImage: {
    width: 35,
    height: 35,
    //borderRadius:10,
    //borderBottomLeftRadius: 10,
    //borderTopLeftRadius: 10,
    borderRadius: 400 / 2,
    resizeMode: 'cover',
    //marginLeft:10
  },
  profilePic: {
    width: 120,
    height: 120,
    marginLeft: 15,
    marginTop: 15,
    borderRadius: 400 / 2,
  },
  body: {
    marginLeft: 10,
    marginRight: 10
  },
  name: {
    fontWeight: 'bold',
    fontSize: 25,
    marginTop: 20,
  },
  functie: {
    fontSize: 15,
  },
  nameView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    //paddingHorizontal: 24,
  },
  starRating: {
    marginTop: 20,
  },
  descriptionTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 20,
    marginBottom: 10
  },
  descriptionBody: {
    fontSize: 15,
    marginTop: 20,
  },
  lucrareImage: {
    width: 120,
    height: 220,
    marginLeft: 12,
    marginTop: 15,
  },
  reviewerText: {
    // marginLeft: 10,
    // marginRight: 10
  },
  reviewerNameAndStars: {
    flexDirection: 'column',
  },
});
